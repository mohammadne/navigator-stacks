import 'package:dina_qc/UI/4_app/page_1.dart';
import 'package:dina_qc/UI/4_app/see_registers.dart';
import 'package:dina_qc/UI/4_app/subPages/sub0.dart';
import 'package:dina_qc/provider/route.dart';
import 'package:dina_qc/provider/user.dart';

import 'package:dina_qc/widgets/exit.dart';
import 'package:dina_qc/widgets/line.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'mainPages/main_0.dart';
import 'mainPages/main_1.dart';
import 'mainPages/main_2.dart';

class Dinawin extends StatefulWidget {
  static const String routeName = '/Dinawin';
  @override
  _DinawinState createState() => _DinawinState();
}

class _DinawinState extends State<Dinawin> {
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  int _currentTab;
  Widget _currentMainPage;
  Widget _currentSubPage;
  List _mainPage;
  List _subPages;
  bool showMainPages = true;

  @override
  void initState() {
    super.initState();
    Provider.of<UserProvider>(context , listen: false).initialSetUp();
    _mainPage = [
      Main0(),
      Main1(),
      Main2(),
    ];
    _subPages = [
      Sub0(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    _currentTab = Provider.of<RouteProvider>(context).currentMainRoute;
    _currentMainPage =
        _mainPage[Provider.of<RouteProvider>(context).currentMainRoute];
    var isThereSub = Provider.of<RouteProvider>(context).currentSubRoute;
    if (isThereSub != null) _currentSubPage = _subPages[isThereSub];
    showMainPages = Provider.of<RouteProvider>(context).showMainPage;

    return WillPopScope(
      onWillPop: () => exitApp(
        context: context,
      ),
      child: Scaffold(
        key: _key,
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            showMainPages == true ? _currentMainPage : _currentSubPage,
            Line(
              alignment: Alignment.bottomCenter,
            ),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentTab,
          onTap: (int index) {
            _currentTab = index;
            _currentMainPage = _mainPage[index];
            Provider.of<RouteProvider>(context)
                .changeBetweenMainPage(newRoute: index);
          },
          backgroundColor: Colors.white,
          selectedItemColor: Colors.orange,
          unselectedItemColor: Colors.black26,
          elevation: 0,
          items: [
            BottomNavigationBarItem(
              title: Text("حساب کاربری") ,
              icon: Icon(Icons.sim_card),
            ),
            BottomNavigationBarItem(
              title: Text("خانه"),
              icon: Icon(Icons.signal_cellular_off),
            ),
            BottomNavigationBarItem(
              title: Text("پروفایل"),
              icon: Icon(Icons.featured_video),
            ),
          ],
        ),
      ),
    );
  }
}