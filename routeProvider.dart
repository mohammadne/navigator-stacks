import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class RouteProvider with ChangeNotifier {
  bool _showMainPage = true;
  int _currentMainRoute = 1;
  bool get showMainPage => _showMainPage;
  int get currentMainRoute => _currentMainRoute;

  List _subStackTrace = [];
  List _subStackTraceType = [];
  int get currentSubRoute => _subStackTrace.isEmpty? null : _subStackTrace.last;
  int get currentSubRouteType => _subStackTraceType.last;

  void changeBetweenMainPage({@required int newRoute}) {
    _showMainPage = true;
    _currentMainRoute = newRoute;
    notifyListeners();
  }

  void goToSubPage({@required int subPageNumber, @required subPageNumberType}) {
    _showMainPage = false;
    _subStackTrace.add(subPageNumber);
    _subStackTraceType.add(subPageNumberType);
    notifyListeners();
  }

  void popUpSubPage() {
    print('stack trace before' + _subStackTrace.toString());
    _subStackTrace.removeLast();
    _subStackTraceType.removeLast();
    print('stack trace after' + _subStackTrace.toString());
    if (_subStackTrace.isEmpty) {
      _showMainPage = true;
    } else {
      _showMainPage = false;
    }
    notifyListeners();
  }

  // void setAlbumId(int albumId) {
  //   _albumId = albumId;
  // }

}
